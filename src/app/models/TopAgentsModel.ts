export class AgentInfoModel {
    agentImg: string;
    agentName: string;
    agentAddress: string;
    agentNetPremium: string;
}