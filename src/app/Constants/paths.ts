'use strict';
export const PATH_LOGIN = "login";
export const PATH_FORGOT_PASSWORD = "forgot-password";
export const PATH_TOP_AGENTS = "topAgents";
export const PATH_AGENTS_WITH_ALERT = "agentWithAlerts";
export const PATH_AGENTS_WITH_PERFORMANCE = "agentWithPerformance";
export const PATH_AGENT_DETAIL = "agentDetail";
export const PATH_AGENT_CONTACT_DETAIL = "contactDetail";

export const sideNavRoutePaths = [PATH_TOP_AGENTS, PATH_AGENTS_WITH_ALERT, PATH_AGENTS_WITH_PERFORMANCE];


